const Jimp = require("jimp")
var fs = require('fs')
var logger = fs.createWriteStream('./image.txt', {
  flags: 'w' 
})

let letters = ` .',:^";*!²¤/r(?+¿cLª7t1fJCÝy¢zF3±%S2kñ5AZXG$À0Ãm&Q8#RÔßÊNBåMÆØ@¶`
let divider = Math.ceil(255 / letters.length)
console.log(divider)
// let letters = "゙゚ゝゞへしっくぃぅうぐこいつょとぇづにゎすえごちぉけでさらどずひれぷぞかたぁゟばげほびぱきがぢせはゐぎゆおあぬぜゑぼぽ"


async function start(path){
  try {
    let image = await Jimp.read(path)
    image.greyscale();
    image.resize(2380, 1080);
    // image.scale(2)
    let xMax = image.bitmap.width
    let yMax = image.bitmap.height

    let i = 0

    for (let y = 0; y <= yMax; y++) {
      console.log(`${y} of ${xMax}`)
      if(y !== 0) logger.write('\n')
      for (let x = 0; x <= xMax; x++) {
        let color = image.getPixelColor(x,y)
        let rgb = Jimp.intToRGBA(color)

        // console.log({
        //   letter: letters[Math.round(rgb.r / divider)],
        //   index: Math.round(rgb.r / divider)
        // })
        logger.write(`${letters[Math.round(rgb.r / divider)]}`)
      }
    }
    console.log(i)
  } catch (error) {
    console.log(error)
    console.log("error in start")
  }
}

start("./max.jpeg")


console.log(letters.length)